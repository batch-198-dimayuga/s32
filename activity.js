let http = require("http");

http.createServer(function(request,response){

    if(request.url === '/' && request.method === 'GET'){

        response.writeHead(200,{'Content-Type':'plain/text'});
        response.end("Welcome to Booking System");

    } else if(request.url === "/profile" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'plain/text'});
        response.end("Welcome to your profile!");

    } else if(request.url === "/courses" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'plain/text'});
        response.end("Here's our courses available");

    } else if(request.url === "/addCourse" && request.method === "POST"){

        response.writeHead(200,{'Content-Type':'plain/text'});
        response.end("Add a course to our resources");

    } else if(request.url === "/updateCourse" && request.method === "PUT"){

        response.writeHead(200,{'Content-Type':'plain/text'});
        response.end("Update a course to our resources");

    } else if(request.url === "/archiveCourses" && request.method === "DELETE"){

        response.writeHead(200,{'Content-Type':'plain/text'});
        response.end("Archive courses to our resources");

    }

}).listen(4000);

console.log("This server is on localhost:4000");